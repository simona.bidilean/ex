<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class ExController extends AbstractController
{
    /**
     * @Route("/ex", name="ex")
     */
    public function index()
    {
        return $this->render('ex/index.html.twig', [
            'controller_name' => 'ExController',
        ]);
      
    }
}
